enablePlugins(JavaServerAppPackaging, UpstartPlugin, GraalVMNativeImagePlugin)
scalaVersion := "2.13.9"
dockerExposedPorts := Seq(8080)
val AkkaVersion = "2.6.8"
val AkkaHttpVersion = "10.2.10"
libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor-typed" % AkkaVersion,
  "com.typesafe.akka" %% "akka-stream" % AkkaVersion,
  "com.typesafe.akka" %% "akka-http" % AkkaHttpVersion,
  "ch.megard" %% "akka-http-cors" % "1.1.3"
)

// these settings are conflicting
javaOptions in Universal ++= Seq("-J-Xmx64m", "-J-Xms64m", "-jvm-debug 12345")
