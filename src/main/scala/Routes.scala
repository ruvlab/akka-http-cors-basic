import akka.http.scaladsl.server.Directives.{authenticateBasic, complete, path}
import akka.http.scaladsl.server.Route

trait Routes extends AuthenticationComponent{
  val helloRoute: Route = path("hello") {
    authenticateBasic(realm = "secure site", myAuthen) { userName =>
      complete(s"The user is '$userName'")
    }
  }
}
