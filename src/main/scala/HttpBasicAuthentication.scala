import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Route
import ch.megard.akka.http.cors.scaladsl.CorsDirectives._

import scala.concurrent.Future
import scala.util.{Failure, Success}

object HttpBasicAuthentication extends App with AkkaComponent  with Routes {

  val route: Route = cors() { Route.seal { helloRoute } }

  val futureHttp: Future[Http.ServerBinding] = Http()
    .newServerAt("0.0.0.0", 8080)
    .bind(route)

  futureHttp.onComplete {
      case Success(_) => println(s"Started!")
      case Failure(e) => println("Failed to start ... ", e)
    }

}
